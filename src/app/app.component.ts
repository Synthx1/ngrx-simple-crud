import {Component} from '@angular/core';
import {StoreSelectorService} from "./store/store-selector.service";
import {MatDialog} from "@angular/material/dialog";
import {AddTagComponent} from "./add-tag/add-tag.component";
import {Observable} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  list$: Observable<string[]>;

  constructor(
    private readonly matDialog: MatDialog,
    private readonly storeSelectorService: StoreSelectorService,
  ) {
    this.list$ = this.storeSelectorService.list$;
  }

  openAddDialog(): void {
    this.matDialog.open(AddTagComponent, { data: { id: 0 } });
  }

  onDelete(data: string): void {
    console.warn(`delete ${data}`);
  }
}
