import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {StoreDispatcherService} from "../store/store-dispatcher.service";

@Component({
  selector: 'app-add-tag',
  templateUrl: './add-tag.component.html',
  styleUrls: ['./add-tag.component.scss']
})
export class AddTagComponent implements OnInit {
  form: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly storeDispatcherService: StoreDispatcherService,
  ) {
    this.form = this.formBuilder.group({
      tag: ['', Validators.required],
    });
  }

  ngOnInit(): void {
  }

  save(): void {
    if (this.form.invalid) return;

    const { tag } = this.form.value;
    this.storeDispatcherService.add(tag);
  }
}
