import {NgModule} from "@angular/core";
import {ActionReducer, StoreModule} from "@ngrx/store";
import {storeReducer} from "./store.reducer";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";

function logger(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) => {
    console.log('action', action);
    return reducer(state, action);
  };
}

@NgModule({
  imports: [
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: false }),
    StoreModule.forRoot({
      root: storeReducer,
    }, { metaReducers: [logger] }),
  ],
})
export class AppStoreModule {}
