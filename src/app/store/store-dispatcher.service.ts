import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import {storeAction} from "./store.action";

@Injectable({
  providedIn: 'root',
})
export class StoreDispatcherService {
  constructor(private store: Store) {
  }

  add(data: string): void {
    this.store.dispatch(storeAction.add({ data }));
  }
}
