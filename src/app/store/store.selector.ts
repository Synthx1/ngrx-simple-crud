import {createSelector} from "@ngrx/store";
import {StoreState} from "./store.state";

const selectState = (state: { root: StoreState }) => state.root;
const list = createSelector(selectState, state => state.list);
const count = createSelector(selectState, state => state.list.length);

export const storeSelector = {
  list,
  count,
};
