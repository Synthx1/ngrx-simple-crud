import {Action, createReducer, on} from "@ngrx/store";
import {StoreState} from "./store.state";
import {storeAction} from "./store.action";

export const initialStoreState: StoreState = {
  list: [],
};

export const storeReducer = createReducer(
  initialStoreState,
  on(storeAction.add, (state, {data}) => {
    return {
      ...state,
      list: state.list.concat(data),
    };
  }),
);
