import {createAction, props} from "@ngrx/store";

const get = createAction('get');
const add = createAction('add', props<{ data: string }>());
const remove = createAction('remove', props<{ data: string }>());
const update = createAction('update', props<{ old: string, data: string }>());

export const storeAction = {
  get,
  add,
  remove,
  update,
};
