import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import {storeSelector} from "./store.selector";
import {StoreState} from "./store.state";

@Injectable({
  providedIn: 'root',
})
export class StoreSelectorService {
  constructor(private readonly store: Store<any>) {
  }

  list$ = this.store.select(storeSelector.list);
  count$ = this.store.select(storeSelector.count);
}
